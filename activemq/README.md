# ActiveMQ Docker Image

### Build docker image

    docker image build -t lapszu/activemq .

### Customieze containter during run-time

Default _admin_ and _user_ passwords are defined in _Dockerfile_.
Environment variables used to customize those passwords are 
ACTIVEMQ_WEBCONSOLE_ADMIN_PASSWORD and ACTIVEMQ_WEBCONSOLE_USER_PASSWORD:

    docker container run -d -e ACTIVEMQ_WEBCONSOLE_ADMIN_PASSWORD=adminsecretpass \
	 -e ACTIVEMQ_WEBCONSOLE_USER_PASSWORD=usersecretpass lapszu/activemq

If You need secure web console with ssl/tls, You can expose port 8162/tcp:

    docker container run -d -p 8162:8162 lapszu/activemq


### Exposed ports

  * 1883  MQTT 
  * 8161  UI
  * 8162  UI over HTTPS
  * 5672  AMQP
  * 61616 JMS
  * 61613 STOMP 
  * 61614 WS 

